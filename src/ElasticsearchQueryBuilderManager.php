<?php

namespace Drupal\inqube;

use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;

/**
 * Provides the Elasticsearch query builder plugin manager.
 */
class ElasticsearchQueryBuilderManager extends DefaultPluginManager {

  /**
   * {@inheritdoc}
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct('Plugin/ElasticsearchQueryBuilder', $namespaces, $module_handler, 'Drupal\inqube\ElasticsearchQueryBuilderInterface', 'Drupal\inqube\Annotation\ElasticsearchQueryBuilder');

    $this->alterInfo('inqube_elasticsearch_query_builder_info');
    $this->setCacheBackend($cache_backend, 'inqube_elasticsearch_query_builder_plugins');
  }

}
