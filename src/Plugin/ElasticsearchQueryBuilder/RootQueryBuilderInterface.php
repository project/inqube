<?php

namespace Drupal\inqube\Plugin\ElasticsearchQueryBuilder;

/**
 * Interface RootQueryBuilderInterface for support root query builder.
 */
interface RootQueryBuilderInterface {

  /**
   * Returns base roots of query.
   *
   * @return array
   *   Array of base roots.
   */
  public function getBaseRoots();

  /**
   * Alters query roots.
   *
   * @param array $base_roots
   *   Array of base roots.
   *
   * @return array
   *   Array of base roots keyed by unique roots.
   */
  public function getAlteredRoots(array $base_roots);

  /**
   * Sets main filter in root query.
   *
   * @param array $query
   *   Root query.
   * @param string $root
   *   Root name.
   */
  public function setQueryRoot(array &$query, $root);

  /**
   * Applies filter to query.
   *
   * @param array $query
   *   Elasticsearch query data.
   * @param array $filter
   *   Filter values.
   * @param string $root
   *   Index root.
   */
  public function applyFilterToRoot(array &$query, array $filter, $root);

  /**
   * Alters each root query once it is created.
   *
   * Used to provide custom logic that can not be implemented with filters.
   *
   * @param array $query
   *   Current root query.
   * @param string $base_root
   *   Base Root.
   * @param string $root
   *   Root name.
   */
  public function alterRootQuery(array &$query, $base_root, $root);

  /**
   * Alters full query.
   *
   * @param array $full_query
   *   Query to apply alteration.
   */
  public function alterFullQuery(array &$full_query);

  /**
   * Get sorting for query.
   *
   * @return array
   *   Sort.
   */
  public function getSort();

}
