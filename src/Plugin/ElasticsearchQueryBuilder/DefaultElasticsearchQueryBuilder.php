<?php

namespace Drupal\inqube\Plugin\ElasticsearchQueryBuilder;

use Drupal\inqube\ElasticsearchQueryBuilderInterface;

/**
 * Class DefaultElasticsearchQueryBuilder base for query builds.
 *
 * @ElasticsearchQueryBuilder(
 *   id = "default",
 *   label = @Translation("Default"),
 *   description = @Translation("Default Elasticsearch query builder; does nothing")
 * )
 *
 * @package Drupal\inqube\Plugin\ElasticsearchQueryBuilder
 */
class DefaultElasticsearchQueryBuilder extends ElasticsearchQueryBuilderPluginBase implements ElasticsearchQueryBuilderInterface {

  /**
   * {@inheritdoc}
   */
  public function buildQuery() {
    return [];
  }

}
