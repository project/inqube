<?php

namespace Drupal\inqube\Plugin\ElasticsearchQueryBuilder;

/**
 * Class BaseIndexRootQueryBuilder for Base index root query building.
 *
 * Base root query implementation for index root based queries.
 *
 * @package Drupal\inqube\Plugin\ElasticsearchQueryBuilder
 */
abstract class BaseIndexRootQueryBuilder extends BaseRootQueryBuilder {

  /**
   * {@inheritdoc}
   */
  public function getAlteredRoots(array $base_roots) {
    $roots = [];
    foreach ($base_roots as $base_root) {
      $roots[$base_root . '_index_' . $this->langCode] = $base_root;
    }

    return $roots;
  }

  /**
   * {@inheritdoc}
   */
  public function setQueryRoot(array &$query, $root) {
    $query['bool']['must'][]['term'] = ['_index' => $root];
  }

}
