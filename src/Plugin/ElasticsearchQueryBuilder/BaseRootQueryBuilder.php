<?php

namespace Drupal\inqube\Plugin\ElasticsearchQueryBuilder;

use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\inqube\QueryBuilderHelperTrait;
use Drupal\views\Plugin\views\display\DisplayPluginBase;
use Drupal\views\ViewExecutable;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class BaseRootQueryBuilder for Base root query building.
 *
 * Root query builder creates separate queries that is capable to apply
 * filters with custom logic per "root" and combines them with should operator.
 * Base root usually refers to entity bundle and root to index.
 *
 * @package Drupal\inqube\Plugin\ElasticsearchQueryBuilder
 */
abstract class BaseRootQueryBuilder extends ElasticsearchQueryBuilderPluginBase implements RootQueryBuilderInterface {

  use QueryBuilderHelperTrait;

  /**
   * Language Manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * Current request.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $request;

  /**
   * Language code.
   *
   * @var string
   */
  protected $langCode;

  /**
   * Clean filter values.
   *
   * @var array
   */
  public $cleanFilterValues = [];

  /**
   * Default Index roots.
   *
   * @var array
   */
  public $baseRoots = [];

  /**
   * Keyword query fields.
   *
   * @var array
   */
  public $keywordFields = [
    'content',
  ];

  /**
   * Keyword query fields.
   *
   * @var array
   */
  public $sortFields = [
    'relevance' => '_score',
    'score' => '_score',
    'title' => 'title',
    'created' => 'created',
    'updated' => 'updated',
  ];

  /**
   * QueryBuilderBase constructor.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   Language manager.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   Current request.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, LanguageManagerInterface $language_manager, Request $request) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->languageManager = $language_manager;
    $this->langCode = $language_manager->getCurrentLanguage()->getId();
    $this->request = $request;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('language_manager'),
      $container->get('request_stack')->getCurrentRequest()
    );
  }

  /**
   * {@inheritdoc}
   */
  public function init(ViewExecutable $view, DisplayPluginBase $display, array &$options = NULL) {
    parent::init($view, $display, $options);
    // Set filter clean values to be used later in process.
    foreach ($this->getFilterValues() as $key => $filter) {
      $this->cleanFilterValues[$key] = static::cleanupFilters($filter);
      if (empty($this->cleanFilterValues[$key])) {
        unset($this->cleanFilterValues[$key]);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getBaseRoots() {
    foreach ($this->baseRoots as $root_key => $root) {
      // Exclude roots if filters not supported.
      if ($this->skipRootOnFilter($root, $this->cleanFilterValues)) {
        unset($this->baseRoots[$root_key]);
      }
    }

    return $this->baseRoots;
  }

  /**
   * {@inheritdoc}
   */
  public function skipRootOnFilter($root, array $filters) {}

  /**
   * {@inheritdoc}
   */
  public function getAlteredRoots(array $base_roots) {
    return $base_roots;
  }

  /**
   * {@inheritdoc}
   */
  public function setQueryRoot(array &$query, $root) {}

  /**
   * {@inheritdoc}
   */
  public function applyFilterToRoot(array &$query, array $filter, $root) {
    if (isset($this->shouldFilters) && array_key_exists($filter['name'], $this->shouldFilters)) {
      $this->addShouldQuery($this->shouldFilters[$filter['name']], $filter, $query);
      return;
    }
    if (isset($this->keywordFilters) && in_array($filter['name'], $this->keywordFilters, FALSE)) {
      $this->addKeywordQuery($filter, $query, $this->keywordFields);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function buildQuery() {
    $full_query = [];
    // Set filters for each root.
    foreach ($this->getAlteredRoots($this->getBaseRoots()) as $root => $base_root) {
      $query = [];
      $this->setQueryRoot($query, $root);
      // Apply filters to root.
      foreach ($this->cleanFilterValues as $filter_name => $filter_values) {
        $this->applyFilterToRoot($query, [
          'name' => $filter_name,
          'value' => $filter_values,
        ], $base_root);
      }
      $this->alterRootQuery($query, $base_root, $root);
      // Insert root query in full query.
      if (!empty($query)) {
        $full_query['body']['query']['bool']['should'][] = $query;
      }
    }
    if ($sort = $this->getSort()) {
      $full_query['body']['sort'] = $sort;
    }
    $this->alterFullQuery($full_query);

    return $full_query;
  }

  /**
   * {@inheritdoc}
   */
  public function getSort() {
    // If custom sorting from url is set then use that.
    if ($sort = $this->sortFromQueryParams()) {
      return $sort;
    }
    // If keyword search is applied then sort by relevance.
    if (isset($this->keywordFilters) && array_intersect($this->keywordFilters, array_keys($this->cleanFilterValues))) {
      return [
        '_score' => ['order' => 'desc'],
      ];
    }
    // Provide default sorting for default query (empty param loading).
    return $this->defaultSort();
  }

  /**
   * Creates sort params from url if set.
   *
   * @return array|bool
   *   Sort params or FALSE.
   */
  public function sortFromQueryParams() {
    $sort_order = 'desc';
    if (($sort_by = $this->request->get('sort_by')) && is_string($sort_by) && array_key_exists($sort_by, $this->sortFields)) {
      if (($sort_order_requested = $this->request->get('sort_order')) && is_string($sort_order_requested) && in_array(strtolower($sort_order_requested), [
        'asc',
        'desc',
      ], FALSE)) {
        $sort_order = strtolower($sort_order_requested);
      }
      return [
        $this->sortFields[$sort_by] => ['order' => $sort_order],
      ];
    }
    return FALSE;
  }

  /**
   * Get default sort.
   *
   * @return array
   *   Sort.
   */
  public function defaultSort() {
    $sort = [];
    foreach ($this->getSortValues() as $sort_by => $order) {
      if (!empty($this->sortFields[$sort_by])) {
        $sort_by = $this->sortFields[$sort_by];
      }
      $sort[] = [
        $sort_by => ['order' => $order],
      ];
    }
    return $sort ?? NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function alterRootQuery(array &$query, $base_root, $root) {}

  /**
   * {@inheritdoc}
   */
  public function alterFullQuery(array &$full_query) {}

}
