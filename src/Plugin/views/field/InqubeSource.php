<?php

namespace Drupal\inqube\Plugin\views\field;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url as CoreUrl;
use Drupal\views\ResultRow;

/**
 * Renders a value from the Elasticsearch result.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("inqube_elasticsearch_source")
 */
class InqubeSource extends Source {

  /**
   * {@inheritdoc}
   */
  public function defineOptions() {
    $options = parent::defineOptions();
    $options['sort_field'] = ['default' => ''];
    $options['load_as_entity'] = ['default' => ''];
    $options['link_to_entity'] = ['default' => FALSE];
    $options['convert_to_link'] = ['default' => FALSE];

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);
    $form['sort_field'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Sort field'),
      '#description' => $this->t('Enter the key in the "_source" field to use for sorting. Leave empty to sort on same field.'),
      '#default_value' => $this->options['sort_field'],
    ];
    $form['load_as_entity'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Entity to load'),
      '#description' => $this->t('Enter entity type to load. Leave empty to display as is.'),
      '#default_value' => $this->options['load_as_entity'],
    ];
    $form['link_to_entity'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Link to entity.'),
      '#default_value' => $this->options['link_to_entity'],
      '#states' => [
        'invisible' => [
          ':input[name="options[load_as_entity]"]' => ['value' => ''],
        ],
      ],
    ];
    $form['convert_to_link'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Convert result as link.'),
      '#description' => $this->t('Convert result value to link. Make sure that the field is a link field'),
      '#default_value' => $this->options['convert_to_link'],
      '#states' => [
        'visible' => [
          ':input[name="options[load_as_entity]"]' => ['value' => ''],
        ],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function clickSort($order) {
    $field = !empty($this->options['sort_field']) ? $this->options['sort_field'] : $this->options['source_field'];
    $this->query->addOrderBy(NULL, $field, $order, '', []);
  }

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $row) {
    // Get value and return if empty.
    $value = (isset($row->_source) && is_array($row->_source)) ? $this->getNestedValue($this->options['source_field'], $row->_source) : '';
    if (!$value) {
      return $value;
    }

    // Handle multiple values.
    if (is_array($value)) {
      // Build unordered list.
      $build = [
        '#theme' => 'item_list',
        '#list_type' => 'ul',
        '#items' => [],
      ];
      foreach ($value as $item) {
        if ($item == 'none') {
          continue;
        }
        // Add items as labels from entities if specified.
        if (!empty($this->options['load_as_entity']) && is_numeric($item)) {
          $entity = \Drupal::entityTypeManager()->getStorage($this->options['load_as_entity'])->load($item);
          $build['#items'][] = $this->options['link_to_entity'] ? $entity->toLink($entity->label())->toString() : $entity->label();
        }
        // Add items as links if specified.
        elseif (!empty($this->options['convert_to_link'])) {
          $markup = $this->convertValueToLink($item);
          $build['#items'][] = $markup;
        }
        // Add plain values if specified.
        else {
          $build['#items'][] = ['#markup' => $item];
        }
      }
      // Return multi-value value.
      return $build;
    }

    // Return label as link or plain as specified.
    if (!empty($this->options['load_as_entity']) && is_numeric($value)) {
      $entity = \Drupal::entityTypeManager()->getStorage($this->options['load_as_entity'])->load($value);
      if (!$entity) {
        return $value;
      }
      return $this->options['link_to_entity'] ? $entity->toLink($entity->label())->toString() : $entity->label();
    }

    // Return link if requested.
    if (!empty($this->options['convert_to_link'])) {
      return $this->convertValueToLink($value);
    }

    // Return plain value if no entity load requested.
    if ($value == 'none') {
      return [];
    }
    return ['#markup' => $value];

  }

  /**
   * Convert this field to a link.
   *
   * @param string $item
   *   The string that is converted to link render array.
   *
   * @return array
   *   The link render array.
   */
  private function convertValueToLink($item) {
    if (!parse_url($item, PHP_URL_SCHEME)) {
      $url = CoreUrl::fromUserInput('/' . ltrim($item, '/'));
    }
    else {
      $url = CoreUrl::fromUri($item);
    }
    return [
      '#type' => 'link',
      '#title' => $item,
      '#url' => $url,
    ];
  }

}
