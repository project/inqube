<?php

namespace Drupal\inqube\Plugin\views\field;

use Drupal\Component\Utility\Unicode;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Url as CoreUrl;
use Drupal\views\ResultRow;

/**
 * Renders a value from the Elasticsearch result.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("inqube_link_elasticsearch_source")
 */
class InqubeLinkSource extends Source {

  /**
   * {@inheritdoc}
   */
  public function defineOptions() {
    $options = parent::defineOptions();
    $options['sort_field'] = ['default' => 'title'];
    $options['trim_length'] = ['default' => 50];

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);
    $form['trim_length'] = [
      '#type' => 'number',
      '#title' => $this->t('Trim length'),
      '#description' => $this->t('Trim link text length'),
      '#required' => TRUE,
      '#min' => 10,
      '#max' => 200,
      '#default_value' => $this->options['trim_length'],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function clickSort($order) {
    $this->query->addOrderBy(NULL, $this->options['source_field'] . '.' . $this->options['sort_field'], $order, '', []);
  }

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $row) {
    // Get value and return if empty.
    $value = (isset($row->_source) && is_array($row->_source)) ? $this->getNestedValue($this->options['source_field'], $row->_source) : '';
    if (empty($value)) {
      return [];
    }

    // Handle multiple values.
    if ($this->isMultidimensional($value)) {
      if (empty($value)) {
        return [];
      }

      // Build unordered list.
      $build = [
        '#theme' => 'item_list',
        '#list_type' => 'ul',
        '#items' => [],
      ];
      foreach ($value as $item) {
        $build['#items'][] = $this->convertValueToLink($item['uri'], $item['title']);
        ;
      }
      // Return multi-value value.
      return $build;
    }

    return $this->convertValueToLink($value['uri'], $value['title']);
  }

  /**
   * Checks if array is multidimensional.
   *
   * @param array $array
   *   Array.
   *
   * @return bool
   *   True if is multidimensional.
   */
  private function isMultidimensional(array $array) {
    foreach ($array as $elm) {
      if (!is_array($elm)) {
        return FALSE;
      }
    }
    return TRUE;
  }

  /**
   * Convert this field to a link.
   *
   * @param string $uri
   *   The string that is converted to link render array.
   * @param string $title
   *   The string that is converted to link render array.
   *
   * @return array
   *   The link render array.
   */
  private function convertValueToLink(string $uri, $title = '') {
    $url = !parse_url($uri, PHP_URL_SCHEME)
      ? CoreUrl::fromUserInput('/' . ltrim($uri, '/'))
      : CoreUrl::fromUri($uri);
    $title = empty($title)
      ? $url->toString()
      : $title;
    return Link::fromTextAndUrl(Unicode::truncate($title, $this->options['trim_length'], FALSE, TRUE), $url)->toRenderable();
  }

}
