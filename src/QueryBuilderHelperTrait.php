<?php

namespace Drupal\inqube;

/**
 * Trait QueryBuilderHelperTrait for Query builder (as a helper).
 *
 * @package Drupal\inqube
 */
trait QueryBuilderHelperTrait {

  /**
   * Filter value cleanup helper.
   *
   * @param mixed $values
   *   Filter values.
   *
   * @return array
   *   Cleaned up values.
   */
  public static function cleanupFilters($values) {
    if (!is_array($values)) {
      return $values;
    }
    $clean_values = [];
    foreach ($values as $value) {
      if ((is_string($value) && $value !== '') || (is_int($value) && $value !== 0)) {
        $clean_values[] = $value;
      }
    }
    return $clean_values;
  }

  /**
   * Cleans date filters.
   *
   * @param array $filters
   *   List of filter names.
   * @param array $cleanFilterValues
   *   List of all filter values.
   */
  public function cleanDateFilters(array $filters, array &$cleanFilterValues) {
    foreach ($filters as $date_filter) {
      if (count($cleanFilterValues[$date_filter]) > 1) {
        $date = \DateTime::createFromFormat('m/d/Y', reset($cleanFilterValues[$date_filter]));
        $date->setTime(0, 0, 0);
        $cleanFilterValues[$date_filter] = $date->getTimestamp();
        continue;
      }
      unset($cleanFilterValues[$date_filter]);
    }
  }

  /**
   * Adds should query.
   *
   * @param string $field
   *   Field to query on.
   * @param array $filter
   *   Filter values.
   * @param array $query
   *   Full query.
   */
  public function addShouldQuery($field, array $filter, array &$query) {
    $root_key = count($query['bool']['must']);
    foreach ($filter['value'] as $value) {
      $query['bool']['must'][$root_key]['bool']['should'][]['term'] = [$field => $value];
    }
  }

  /**
   * Adds keyword query.
   *
   * @param array $filter
   *   Filter values.
   * @param array $query
   *   Full query.
   * @param array $fields
   *   Fields to query.
   */
  public function addKeywordQuery(array $filter, array &$query, array $fields) {
    foreach (preg_split("/[\s,-]+/", $filter['value']) as $keyword) {
      if (strpos($keyword, "'") !== FALSE) {
        $keyword = substr($keyword, 0, strpos($keyword, "'"));
      }
      $query['bool']['must'][]['query_string'] = [
        'query' => $keyword . ' OR ' . $keyword . '* OR  *' . $keyword . '*',
        'fields' => $fields,
      ];
    }
  }

}
